; Make file of PP Core module

api = 2
core = 7.x

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[features][subdir] = "contrib"
projects[features][version] = "2.0"

projects[views][subdir] = "contrib"
projects[views][version] = "3.7"

projects[uuid_features][subdir] = "contrib"
projects[uuid_features][download][type] = "git"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.1"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.3"

projects[panels_breadcrumbs][subdir] = "contrib"
projects[panels_breadcrumbs][download][type] = "git"
projects[panels_breadcrumbs][download][url] = "http://git.drupal.org/project/panels_breadcrumbs.git"
projects[panels_breadcrumbs][download][branch] = "7.x-2.x"
projects[panels_breadcrumbs][patch][] = "https://drupal.org/files/issues/panels_breadcrumbs-2087081-undefined-index-269.patch"

projects[panelizer][subdir] = "contrib"
projects[panelizer][version] = "3.1"

projects[panels_everywhere][subdir] = "contrib"
projects[panels_everywhere][version] = "1.0-rc1"

projects[media][subdir] = "contrib"
projects[media][download][type] = "git"
projects[media][download][branch] = "7.x-2.x"
projects[media][download][revision] = "042f84fd9569df4ea3bafc193db6196bd8158c01"
projects[media][patch][] = "https://drupal.org/files/issues/media-2148869-InsertMedia-global.patch"

projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "2.0-rc4"

projects[date][subdir] = "contrib"
projects[date][version] = "2.7"

projects[ctools_plugins][type] = "module"
projects[ctools_plugins][subdir] = "contrib"
projects[ctools_plugins][download][type] = "git"
